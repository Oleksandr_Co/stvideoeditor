package StVideoEditor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.swing.JProgressBar;

public class AddContactVideoToVideoExecute {
	public AddContactVideoToVideoExecute (String pathVideo, VideoFileProperties videoFileProperties, JProgressBar jProgressBar) {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.S");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		//String timeMark = sdf.format(cal.getTime());
		Long timeMark = cal.getTimeInMillis();
		
		String filePath = videoFileProperties.getVideoFilePath();
		String fileName = videoFileProperties.getVideoFormatName();
		String fileDirectory = videoFileProperties.getVideoFileDirectory();
		String pathAddObjectSave = pathVideo.replace("\\", "\\\\");
		
		File file = new File("ffmpeg");
		String ffmpegpath = file.getAbsolutePath();
   		ffmpegpath = ffmpegpath.substring(0, ffmpegpath.indexOf("ffmpeg"));
   		ffmpegpath = ffmpegpath + "\\res\\ffmpeg.exe";
		String ffmpegPath = ffmpegpath.replace("\\","\\\\");
		
		String path       = filePath.replace("\\", "\\\\");
		String fileDirectorySave = fileDirectory.replace("\\", "\\\\");
		
		String[] command = {ffmpegPath, "-i", path, "-i", pathAddObjectSave, "-filter_complex", "\"[0:v:0][0:a:0][1:v:0][1:a:0]concat=n=2:v=1:a=1[outv][outa]\"", "-map", "\"[outv]\"","-map", "\"[outa]\"", fileDirectorySave + "\\" + "Contact" + timeMark + fileName};
		try {
			Process p = Runtime.getRuntime().exec(command);
			new ExecuteBarClass(jProgressBar, p);
	    } catch (Exception e){
	    	jProgressBar.setString("Something go wrong, try to change parameters!");
            e.printStackTrace();
        }
	}
}
