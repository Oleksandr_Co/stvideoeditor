package StVideoEditor;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.swing.JProgressBar;

public class VideoConvertExecute {
	public  VideoConvertExecute(VideoFileProperties videoFileProperties, JProgressBar jProgressBar) {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.S");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		Long timeMark = cal.getTimeInMillis();
		
		String filePath = videoFileProperties.getVideoFilePath();
		String fileExtension = videoFileProperties.getVideoFileExtension();
		String format     = videoFileProperties.getVideoFormatFile();
		String fileName   = videoFileProperties.getVideoFormatName();
		String fileDirectory = videoFileProperties.getVideoFileDirectory();

		File file = new File("ffmpeg");
		String ffmpegpath = file.getAbsolutePath();
   		ffmpegpath = ffmpegpath.substring(0, ffmpegpath.indexOf("ffmpeg"));
   		ffmpegpath = ffmpegpath + "\\res\\ffmpeg.exe";
		String ffmpegPath = ffmpegpath.replace("\\","\\\\");
	    
	    String path       = filePath.replace("\\","\\\\");
	    String fileDirectorySave = fileDirectory.replace("\\", "\\\\");
	  
	    int i = fileName.lastIndexOf('.');
	    String name = fileName.substring(0,i);
	    String fileSave = fileDirectorySave + "\\"+ "Converted" + timeMark + name + "." + format;
	    
	    String fileExtensionSave = fileExtension.substring(fileExtension.indexOf("(") + 1, fileExtension.indexOf(")"));
	    String videoVolume = videoFileProperties.getVideoFileAudioVolume();
	    new Thread() {
	    	public void run() {			    
			    try
			    {	
			    	if(format == "webm" ) {
				    	String[] execCommandString = {ffmpegPath, "-i", path, "-vf",  "scale=" + fileExtensionSave, "-c:v", "libvpx",  "-crf", "10", "-b:v", "1M", "-c:a", "libvorbis", "-af", "volume=" + videoVolume + "dB", fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	} else if (format == "ogg" || format == "ogv") {
			    		String[] execCommandString = {ffmpegPath, "-y", "-i", path, "-codec:v", "libtheora", "-qscale:v", "6", "-s", fileExtensionSave, "-codec:a", "libvorbis", "-qscale:a", "6", "-af", "volume=" + videoVolume + "dB",  fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	}  else if (format == "wmv") {
			    		String[] execCommandString = {ffmpegPath, "-i", path, "-vf",  "scale=" + fileExtensionSave, "-c:v", "wmv2", "-b:v", "2048k", "-c:a", "wmav2", "-b:a", "192k", "-af", "volume=" + videoVolume + "dB", fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	} else if (format == "3gp") {
			    		String[] execCommandString = {ffmpegPath, "-y", "-i", path, "-r", "20", "-s", "352x288", "-b", "400k", "-acodec", "aac", "-strict", "experimental", "-ac", "1", "-ar", "8000", "-ab", "24k", fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	} else if (format == "mpg" || format == "mpeg") {
			    		String[] execCommandString = {ffmpegPath, "-y", "-i", path, "-vf",  "scale=" + fileExtensionSave, "-q:v", "5", "-q:a", "5", "-af", "volume=" + videoVolume + "dB", "-f", "dvd", fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	} else if (format == "mts") {
			    		String[] execCommandString = {ffmpegPath, "-y", "-i", path, "-vcodec", "libx264", "-r", "30000/1001", "-b:v", "21M", "-vf",  "scale=" + fileExtensionSave, "-acodec", "ac3","-af", "volume=" + videoVolume + "dB", fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	} else {
				    	String[] execCommandString = {ffmpegPath, "-i", path, "-vf",  "scale=" + fileExtensionSave, "-q:v", "5", "-q:a", "5", "-af", "volume=" + videoVolume + "dB", fileSave};
				    	Process p = Runtime.getRuntime().exec(execCommandString);
				    	new ExecuteBarClass(jProgressBar, p);
			    	}
			    	
				} catch (Exception e){
		            e.printStackTrace();
		            jProgressBar.setString("Something go wrong, try to change parameters!");
		        } 
	    	}
	    }.start();
	}
}
