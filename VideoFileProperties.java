package StVideoEditor;

public class VideoFileProperties {
	
	private static String VideoFilePath      = null;
	private static String VideoFileExtension = "(1280x720)";
	private static String VideoFileFormat    = "mp4";
	private static String VideoFileName      = null;
	private static String VideoFileDiractory = null;
	private static String VideoFileAudioVolume = "0"; 
	private static String AddImageOrVideoPosition  = "TopLeftCorner(0:0)";
	private static String SpeedOfVideo = "1";
	private static String VideoAspectRation = "16:9";
	
	public void setVideoAspectRation(String ration) {
		this.VideoAspectRation = ration;
	}
	
	public void setAddImageOrVideoPosition(String position) {
		this.AddImageOrVideoPosition = position;
	}
	
	public void setSpeedOfVideo(String speed) {
		this.SpeedOfVideo = speed;
	}
	
	public void setVideoFilePath(String path) {
		this.VideoFilePath = path;
	}
	
	public void setVideoFileExtension(String extension) {
		this.VideoFileExtension = extension;
	}
	
	public void setVideoFileFormat(String format) {
		this.VideoFileFormat = format;
	}
	
	public void setVideoFileName(String name) {
		this.VideoFileName = name;
	}
	
	public void setVideoFileAudioVolume(String VideoVolume) {
		this.VideoFileAudioVolume = VideoVolume;
	}
	
	public void setVideoFileDirectory(String diractoryName) {
		this.VideoFileDiractory = diractoryName;
	}
	
	public String getVideoFilePath() {
		return this.VideoFilePath;
	}
	
	public String getVideoFileExtension() {
		return this.VideoFileExtension;
	}
	
	public String getVideoFormatFile() {
		return this.VideoFileFormat;
	}
	
	public String getVideoFormatName() {
		return this.VideoFileName;
	}
	
	public String getVideoFileDirectory() {
		return this.VideoFileDiractory;
	}
	
	public String getVideoFileAudioVolume() {
		return this.VideoFileAudioVolume;
	}
	
	public String getAddImageOrVideoPosition() {
		return this.AddImageOrVideoPosition;
	}
	
	public String getSpeedOfVideo() {
		return this.SpeedOfVideo;
	}
	
	public String getVideoAspectRation() {
		return this.VideoAspectRation;
	}
}
